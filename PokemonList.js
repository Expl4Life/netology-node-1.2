class PokemonList extends Array {
	add(name, level = 0, foundStatus = true){
		const Pokemon = require("./Pokemon");
		this.push(new Pokemon(name, level, foundStatus));
	}
	show(){
		this.forEach((pokemon)=>{
			console.log(`Имя покемона: ${pokemon.name}. Уровень: ${pokemon.level}. Найден: ${pokemon.foundStatus}`);
		})
		console.log(`Общее количество покемонов в списке = ${this.length}`);
	}
	max(){
		let allLevels = this.map(pokemon => pokemon.level);
		let maxLevel = Math.max(...allLevels);
		let indexPokemon = allLevels.findIndex(level => level == maxLevel);
		let pokemon = this[indexPokemon];
		return pokemon
	}
};

module.exports = PokemonList;