class Pokemon {
	constructor(name, level = 0, foundStatus = true) {
		this.name = name;
		this.level = level;
		this.foundStatus = foundStatus;
	}
	show(){
		console.log(`Меня зовут ${this.name}, мой уровень - ${this.level}.`);
	}
};

module.exports = Pokemon;
