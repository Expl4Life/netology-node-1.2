const hide = (path, PokList) => {
	const PokemonList = randomPokList(3);
	//Creating folders
	const fs = require("fs");
	fs.mkdir(path, createMainFolder);
	// function creates randomlist of Pokemons
	function randomPokList(quantity) {
		const pokObjects = require("./getObj");
		let PokemonList = new PokList(...pokObjects);
		if (PokemonList.length > quantity) {
			let shoudSpliced = PokemonList.length - quantity;
			const random = require("./random");
			for(let i=0; i < shoudSpliced; i++) {
				PokemonList.splice(random(0, PokemonList.length-1), 1);
			}
		}
		PokemonList.forEach(pokemon => pokemon.foundStatus = false);
		return PokemonList;
	};
	function createMainFolder(err){
		if (err) throw err;
		console.log("Папка field создана");
		for(let i = 1; i <= 10; i++) {
			if(i > 9) {
				fs.mkdir(`./field/${i}`, createDirMoreThan9(err,i));
			} else {
				fs.mkdir(`./field/0${i}`, createDirLessThan10(err,i));
			}
		};
		PokemonList.forEach(writeTXT);
	};
	function createDirMoreThan9(err, i) {
		if (err) throw err;
		console.log(`Папка ${i} создана`);
	};
	function createDirLessThan10(err,i) {
		if (err) throw err;
		console.log(`Папка 0${i} создана`);
	};
	function writeTXT(pokemon,i){
		i+=2;
		let conf = { encoding: 'utf8' };
		let content = pokemon.name + " | " + pokemon.level;
		fs.writeFile(`./field/0${i}/pokemon.txt`, content, conf,  err => {
		    if (err) throw err;
		    console.log(`Файл pokemon.txt сохранен`);
		});
	};
	return PokemonList;
};
const seek = (path) => {
	function getData(callback) {
		const fs = require("fs");
		const conf = {encoding: "utf-8"};
		const regString = /([A-Z])\w+/g;
		const regNumber = /([0-9])+/g;
		let name, level;
		const Pokemon = require("./Pokemon");
		const PokList = require("./PokemonList");
		let PokemonList;
		const pokemons = [];
		for(let i=1; i <=10; i++) {
			fs.readFile(`./field/0${i}/pokemon.txt`, conf, (err, content) => {
				if(i > 9) {
					if(err) return console.log(`Файл pokemon.txt по адресу: ./field/${i}/ -  не был найден`);
					console.log(`Файл pokemon.txt по адресу: ./field/${i}/ НАЙДЕН`);
				} else {
					if(err) return console.log(`Файл pokemon.txt по адресу: ./field/0${i}/ -  не был найден`);
					console.log(`Файл pokemon.txt по адресу: ./field/0${i}/ НАЙДЕН`);
				}
				name = content.match(regString)[0];
				level = content.match(regNumber)[0];
				pokemons.push(new Pokemon(name, level));
				if (pokemons.length == 3) {
					PokemonList = new PokList(...pokemons);
					callback(null, PokemonList);
				}
			});
		}
	}
	getData((err, PokemonList)=> {
		if (err) throw error;
		console.log(PokemonList);
		return PokemonList;
	});
};

module.exports = {
	hide,
	seek
};



