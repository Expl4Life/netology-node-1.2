const Pokemon = require("./Pokemon");
const PokemonList = require("./PokemonList");
const pokemons = require("./pokemons.json");
const pokemonsObj = pokemons.map(pokemon => {
	return new Pokemon(pokemon.name, pokemon.level);
});

module.exports = pokemonsObj;